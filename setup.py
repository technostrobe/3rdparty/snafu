from setuptools import setup

setup(
 name="snafu",
 version="0.1",
 python_requires=">=3.7",
 description="Sequential Streaming Networked Atomic Firmware Updater (snafu)",
 author="ExMakhina",
 license="MIT",
 keywords="",
 install_requires=[],
 packages=[
  "exmakhina.snafu",
  "exmakhina.snafu.platform",
  "exmakhina.snafu.server",
 ],
)
