#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Test for MBR stuff

import logging
import io

logger = logging.getLogger(__name__)


commands = """
openssl genpkey -algorithm Ed25519 > ca-private.pem
openssl pkey -in ca-private.pem -pubout -out ca-public.pem
openssl req -key ca-private.pem -new -x509 -subj "/CN=The CA" -days 36500 -out ca-cert.pem
openssl x509 -in ca-cert.pem -text -noout

openssl ecparam -genkey -name secp384r1 -noout -out private.pem
openssl ec -in private.pem -pubout -out public.pem
openssl req -key private.pem -new -subj "/CN=The signer" -out cert.csr
openssl x509 -req -in cert.csr -CA ca-cert.pem -CAkey ca-private.pem -CAcreateserial -days 36500 -out cert.pem
cat private.pem cert.pem > cert+private.pem

openssl ecparam -genkey -name secp384r1 -noout -out private2.pem
openssl ec -in private2.pem -pubout -out public2.pem
openssl req -key private2.pem -new -subj "/CN=The signer" -out cert2.csr
openssl x509 -req -in cert2.csr -CA ca-cert.pem -CAkey ca-private.pem -CAcreateserial -days 36500 -out cert2.pem
cat private2.pem cert2.pem > cert+private2.pem

tar c LICENSES > truc.tar
ln -sf truc.tar test

echo Signing
#
cat test | openssl cms -sign -signer cert+private.pem -binary -outform DER -md sha512 -subject "pouet pouet" -to "Toto" -nodetach > test-cms-sign-ca.cms


echo Printing CMS
openssl cms -cmsout -inform DER -in test-cms-sign-ca.cms -print

echo Verifying
openssl cms -verify -CAfile ca-cert.pem -in  test-cms-sign-ca.cms -inform DER
""".strip()


def test_gen():
	from .cms import cms_verify

	import subprocess
	for cmd in commands.splitlines():
		cmd = [
		"sh", "-c", cmd,
		]
		subprocess.run(cmd)

	with io.open("test-cms-sign-ca.cms", "rb") as fi:
		gen = cms_verify(fi, cacert="ca-cert.pem")

		with next(gen) as fifi:
			fifi.read()

		try:
			next(gen)
		except StopIteration:
			pass
