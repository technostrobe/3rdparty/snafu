#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Helpers

from typing import (
 BinaryIO,
 Sequence,
)

import os
import logging
import mmap
import subprocess
import shlex


logger = logging.getLogger(__name__)


def list2cmdline(lst: Sequence[str]):
	return " ".join(shlex.quote(x) for x in lst)


def run(cmd: Sequence[str], **kw):
	"""
	Run a command line.

	For Reasons, unlike subprocess.run, checking of the return code
	is done unless specified otherwise.
	"""
	if isinstance(cmd, str):
		assert kw.get("shell")
		logger.debug("> %s", cmd)
	else:
		logger.debug("> %s", list2cmdline(cmd))

	kw["check"] = kw.get("check", True)

	return subprocess.run(cmd, **kw)


def runstr(cmd: str, **kw):
	"""
	Run a command line passed as string, without spawning a shell
	"""
	return run(shlex.split(cmd), **kw)


def stream_to_disk(fobj: BinaryIO, size: int, location: str, offset: int=0):
	"""
	Write stream contents to disk.

	:param fobj: file-like to read from
	:param size: amount to transfer
	:param location: where to write

	The contents are written in a memory efficient fashion.
	"""
	regular_chunk_size = 512 * 1024
	chunk_size = regular_chunk_size
	remaining = size
	last_read_pending = b""
	idx_chunk = 0

	logger.info("Downloading payload to %s", location)

	fo = os.open(location, os.O_WRONLY|os.O_DIRECT)

	os.lseek(fo, offset, os.SEEK_SET)

	m = mmap.mmap(-1, chunk_size)
	try:
		while remaining:
			if remaining < chunk_size:
				chunk_size = remaining

			if last_read_pending:
				toread = chunk_size - len(last_read_pending)
				chunk = last_read_pending
				if toread > 0:
					chunk += fobj.read(toread)
				last_read_pending = b""
			else:
				chunk = fobj.read(chunk_size)

			logger.debug("Writing chunk #%s of size %s", idx_chunk, chunk_size)

			if chunk_size != regular_chunk_size:
				logger.debug("Resize mmap to different chunk size: %s", chunk_size)
				m = mmap.mmap(-1, chunk_size)

			m.seek(0)
			m.write(chunk)

			written = os.write(fo, m)

			if written != chunk_size:
				last_read_pending = chunk[written:]

			remaining -= written
			idx_chunk += 1
		os.fsync(fo)
	finally:
		os.close(fo)

	logger.debug("Done writing data to: %s", location)

	# TODO trim remainder


def blockdev_by_partlabel(name):
	"""
	:return: full path of partition with name `name`
	"""

	link = f"/dev/disk/by-partlabel/{name}"
	result = os.readlink(link)
	return os.path.normpath(os.path.join(os.path.dirname(link), result))
