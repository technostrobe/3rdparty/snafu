#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Demo platform

import os
import logging


from ..unwrapping import LimitedInputStream
from ..imagewalker_tar import walk
from ..cms import CMSVerifyingStream
from ..system import (
 stream_to_disk,
 blockdev_by_partlabel,
)
from .uboot import (
 fw_setenv,
 fw_getenv,
)
from .systemd import delayed_reboot


logger = logging.getLogger(__name__)


def initialize():
	pass


def process_data(self):
	logger.info("Processing upgrade payload!")
	content_len = int(self.headers.get('Content-Length', 0))
	if not content_len:
		raise ValueError("Got payload with no size!")

	conf = self.server.config

	with (
	 LimitedInputStream(self.rfile, content_len) as p,
	 CMSVerifyingStream(p, cacert=conf["keys"]["ca-certificate"]) as q,
	 ):

		for name, size, fi in walk(q):
			logger.info("name=%s, size=%s", name, size)

			with fi:
				fi.read(size)

	self.server.update_ready = True
	self.server.update_slot = 1

	return "OK"


def process_go(self):
	update_ready = getattr(self.server, "update_ready", False)

	if not update_ready:
		raise RuntimeError("Not ready for system upgrade! Need to process upgrade payload first!")

	slot = self.server.update_slot

	if os.getuid() == 0:
		delayed_reboot()

	return "OK"


def process_version(self):
	return "TODO"
