#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Helpers for MBR booting (with active partition switchover)

"""

Reference: https://en.wikipedia.org/wiki/Master_boot_record

"""

from typing import (
 Mapping,
 Tuple,
 Sequence,
)
import io, os
import logging
import mmap
import collections

from ..system import stream_to_disk


logger = logging.getLogger(__name__)

SECTOR_SIZE = 512
MBR_SIZE = SECTOR_SIZE
PTES_OFF = 0x1be
PTE_SZ = 16

PartInfo = collections.namedtuple("PartInfo", "bootable, offset, size, code")


def mbr_parts(path) -> Mapping[int,PartInfo]:
	"""
	:return: 
	"""
	ret = {}

	with io.open(path, "rb") as fi:
		mbr = fi.read(MBR_SIZE)

	for idx_part in range(4):
		pte_off = PTES_OFF + PTE_SZ * idx_part
		pte = mbr[pte_off:pte_off+PTE_SZ]

		code = pte[4]
		start = int.from_bytes(pte[8:12], "little")
		size = int.from_bytes(pte[12:16], "little")
		bootable = pte[0] == 0x80

		ret[idx_part] = PartInfo(bootable, start*SECTOR_SIZE, size*SECTOR_SIZE, code)

	return ret


def mbr_bootslot_get(path) -> Tuple[int,Sequence[int]]:
	"""
	Find current bootslot on MBR
	"""

	parts = mbr_parts(path)

	bootables = []
	for num, (bootable, offset, size, typecode) in parts.items():
		if bootable:
			bootables.append(num)

	curpartidx = bootables[0]
	curpart = parts[curpartidx]

	logger.debug("Reference: %d %s", bootables[0], curpart)

	if len(bootables) != 1:
		raise RuntimeError(f"Should only have one bootable but got {bootables}")

	slots = []

	for num, part in parts.items():
		if (part.size, part.code) == (curpart.size, curpart.code):
			slots.append(num)

	logger.info("Slot parts: %s", slots)

	return slots.index(curpartidx), slots


def mbr_bootslot_set(path, slot: int):
	"""
	Set MBR boot slot

	Use direct/sync writes which translates to SCSI FUA,
	and eMMC reliable write.
	"""

	_, slotparts = mbr_bootslot_get(path)

	with io.open(path, "rb") as fi:
		mbr = bytearray(fi.read(MBR_SIZE))

	for slot_, idx_part in enumerate(slotparts):
		offset = PTES_OFF + PTE_SZ * idx_part
		if slot == slot_:
			value = 0x80
		else:
			value = 0x00
		mbr[offset] = value

	fo = os.open(path, os.O_WRONLY|os.O_DIRECT|os.O_SYNC)
	m = mmap.mmap(-1, MBR_SIZE)
	m.write(mbr)
	try:
		written = os.write(fo, m)
		if written != MBR_SIZE:
			raise IOError("MBR write error")
	finally:
		os.close(fo)


def mbr_bootloader_update(path, fi, size):
	"""
	Update MBR bootloader from stream
	"""

	updated = False

	slot_cur, slotparts = mbr_bootslot_get(path)
	slot_nxt = (slot_cur + 1) % len(slotparts)
	parts = mbr_parts(path)

	slotpart = slotparts[slot_nxt]
	nextpart = parts[slotpart]
	offset = nextpart[1]

	logger.debug("Will write to next partition %d at offset %d",
	 slotpart, offset)

	stream_to_disk(fi, size, path, offset)

	return slot_nxt


__all__ = (
 "mbr_bootloader_update",
)
