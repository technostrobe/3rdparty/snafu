#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Test for MBR stuff

import logging
import contextlib
import subprocess
import os

logger = logging.getLogger(__name__)

@contextlib.contextmanager
def disk_image(name="disk"):
	cmd = [
	 "truncate",
	 "--size=1GiB",
	 name,
	]
	subprocess.run(cmd, check=True)
	cmd = [
	 "parted",
	 "--script",
	 name,
	 "mklabel msdos",
	 "mkpart primary fat32 1MiB 2MiB",
	 "mkpart primary fat32 2MiB 3MiB",
	 "toggle 1 boot",
	]
	subprocess.run(cmd, check=True)
	yield name
	os.unlink(name)


def test_mbr_get():
	from .boot_mbr import mbr_parts
	with disk_image() as disk:
		parts = mbr_parts(disk)
		for idx_part, part in parts.items():
			logger.info("%d: %s", idx_part, part)


def test_bootslot_get():
	from .boot_mbr import mbr_bootslot_get
	with disk_image() as disk:
		slot, slotparts = mbr_bootslot_get(disk)
		assert slot == 0


def test_bootslot_set():
	from .boot_mbr import mbr_bootslot_get
	from .boot_mbr import mbr_bootslot_set

	with disk_image() as disk:
		for x in range(2):
			mbr_bootslot_set(disk, x)
			slot, slotparts = mbr_bootslot_get(disk)
			logger.debug("Slot: %s", slot)
			assert slot == x


def test_update():
	import io
	from .boot_mbr import mbr_bootloader_update
	from .boot_mbr import mbr_bootslot_set
	d = b"p" * 512
	buf = io.BytesIO(d)
	buf.seek(0)
	with disk_image() as disk:
		slot_nxt = mbr_bootloader_update(disk, buf, len(d))
		mbr_bootslot_set(disk, slot_nxt)
