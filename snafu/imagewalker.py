#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# image stream walker

import logging

from .unwrapping import LimitedInputStream


def walk(fi):
	process = {
	 "size": lambda x: int(x),
	}
	while True:
		info = {}
		while True:
			line = fi.readline()
			if not line:
				return
			line = line.decode("utf-8").rstrip()
			if not line:
				break
			k, v = line.split(": ", 1)
			info[k] = process.get(k, lambda x: x)(v)
		name = info["name"]
		size = info["size"]
		stream = LimitedInputStream(fi, size=size)
		yield name, size, stream
