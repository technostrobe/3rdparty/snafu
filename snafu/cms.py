#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# CMS verification

"""

References:

- https://en.wikipedia.org/wiki/Cryptographic_Message_Syntax
- man ssl-openssl-cms

"""

import sys, io, os
import tempfile
import logging
import subprocess
import hashlib

from .system import list2cmdline
from .asn1der import Node
from .peeker import FakeBytes
from .unwrapping import LimitedInputStream


logger = logging.getLogger(__name__)


class HashComputingStream(LimitedInputStream):
	"""
	Input stream which will compute a running hash
	"""
	def __init__(self, fi, size, name=None, h_ctor=None):
		super().__init__(fi, size, name)
		if name is None:
			name = f"(stream size={size} fi={fi})"
		if h_ctor is None:
			h_ctor = hashlib.sha512
		self._h = h_ctor()

	def read(self, length=None):
		data = super().read(length)
		self._h.update(data)
		return data

	def digest(self):
		return self._h.digest()


def cms_verify(fi, cacert):
	"""
	CMS verification generator

	Yields a HashComputingStream then StopIteration in normal operation;
	will raise in case of issue.
	"""

	der = FakeBytes(fi)

	root = Node(der, 0)

	node_dgstalg = root[1][0][1][0][0]
	logger.debug("Digest alg @ %s", node_dgstalg)
	dgstalg = node_dgstalg.v().hex()
	logger.debug("Digest alg encoded OID: %s", dgstalg)

	hash_eoid_to_ctor = {
	 "608648016503040203": (hashlib.sha512, "-sha512"),
	 "608648016503040202": (hashlib.sha384, "-sha384"),
	 "608648016503040201": (hashlib.sha256, "-sha256"),
	}
	h_ctor, h_ossl = hash_eoid_to_ctor[dgstalg]

	node_data = root[1][0][2][1][0]
	logger.debug("Digest @ %s", node_data)

	if der._len != node_data.vpos():
		raise RuntimeError(f"{der._pos} vs. {node_data.vpos()}")

	data_stream = HashComputingStream(fi, size=node_data.l(), h_ctor=h_ctor)
	yield data_stream

	h = data_stream.digest()

	logger.debug("Measured digest: %s", h.hex())

	der = FakeBytes(fi)
	node_certificates = Node(der, 0)
	logger.debug("Certificates: @ %s", node_certificates)
	node_signerinfos = node_certificates.next()

	if 1:
		certificate = node_certificates.v()
		logger.debug("Checking certificate: %s", certificate.hex())

		cmd1 = [
		 "openssl",
		  "x509",
		  "-inform", "der",
		  "-outform", "pem",
		]
		cmd2 = [
		 "openssl",
		  "verify",
		  "-CAfile", cacert,
		]
		cmd = "{} | {}".format(list2cmdline(cmd1), list2cmdline(cmd2))

		proc = subprocess.Popen(cmd,
		 shell=True,
		 stdin=subprocess.PIPE,
		 stdout=subprocess.PIPE,
		 stderr=subprocess.PIPE,
		)

		out, err = proc.communicate(certificate)
		logger.debug("out: %s", out)
		if err:
			logger.debug("err: %s", err)
		if proc.wait() != 0:
			raise RuntimeError(f"Certificate verification failure: {err}")

		# extract pub key from certificate, to verify the signature
		cmd = "openssl x509 -inform DER -pubkey -noout".split()
		proc = subprocess.Popen(cmd,
		 stdin=subprocess.PIPE,
		 stdout=subprocess.PIPE,
		 stderr=subprocess.PIPE,
		)
		out, err = proc.communicate(certificate)
		cert_pubkey = out

	logger.debug("Signer info @ %s", node_signerinfos)
	signerInfos = node_signerinfos.v()
	logger.debug("Signer info: %s", signerInfos.hex())

	node_signedAttrs = node_signerinfos[0][3]
	logger.debug("signedAttrs @ %s", node_signedAttrs)
	# Replace IMPLICIT [0] with EXPLICIT SET OF
	# https://www.rfc-editor.org/rfc/rfc5652#section-5.4
	signedAttrs = b"\x31" + node_signedAttrs.lv()
	logger.debug("signedAttrs: %s", signedAttrs.hex())

	node_dgst = node_signedAttrs[2][1][0]
	logger.debug("Digest @ %s", node_dgst)
	dgst = node_dgst.v().hex()
	logger.debug("Digest: %s", dgst)

	if dgst != h.hex():
		raise RuntimeError("Integrity check failure")

	logger.debug("Computed digest is same as signature's, now verify signature")

	node_sigalg = node_signedAttrs.next()
	node_sig = node_sigalg.next()

	with tempfile.TemporaryDirectory() as tmpdirname:
		j = lambda x: os.path.join(tmpdirname, x)
		with io.open(j("sig"), "wb") as fo:
			fo.write(node_sig.v())

		with io.open(j("signedattrs"), "wb") as fo:
			fo.write(signedAttrs)

		with io.open(j("cert-read-key.pem"), "wb") as fo:
			fo.write(cert_pubkey)


		logger.debug("Verify signature")
		cmd = [
		 "openssl",
		  "dgst",
		   "-verify",
		   j("cert-read-key.pem"),
		   "-signature", j("sig"),
		   h_ossl,
		   j("signedattrs"),
		]
		proc = subprocess.Popen(cmd,
		 stdout=subprocess.PIPE,
		 stderr=subprocess.PIPE,
		)
		out, err = proc.communicate()
		logger.debug("out: %s", out)
		if err:
			logger.debug("err: %s", err)

		if proc.wait() != 0:
			raise RuntimeError(f"Signature verification failed: {out}")


class CMSVerifyingStream:
	"""
	Input stream adapter, consumes a CMS signed message,
	and produces the payload; will except at the end if
	verification fails.
	"""
	def __init__(self, fi, cacert):
		self._pos = 0
		self._gen = cms_verify(fi, cacert)

	def __enter__(self):
		self._fi = next(self._gen)
		return self

	def __exit__(self, *args):
		try:
			next(self._gen)
		except StopIteration:
			pass

	def read(self, length):
		return self._fi.read(length)
